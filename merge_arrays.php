<?php
$a = [
    1,
    4,
    7,
    7,
    12,
    12
];
$b = [
    3,
    7,
    12,
    15,
    15
];

/**
 *
 * @param integer[] $a
 * @param integer[] $b
 * @return integer[]
 */
function merge (array $a, array $b) {
    $result = [];
    $last = null;

    $i = $j = 0;
    while ($i < count($a) || $j < count($b)) {
        $candidate = null;

        if ($i == count($a)) {
            $candidate = $b[$j];
            $j++;
        } elseif ($j == count($b)) {
            $candidate = $a[$i];
            $i++;
        } elseif ($a[$i] <= $b[$j]) {
            $candidate = $a[$i];
            $i++;
        } else {
            $candidate = $b[$j];
            $j++;
        }

        if ($candidate == $last) {
            continue;
        }

        $last = $candidate;
        $result[] = $candidate;
    }

    return $result;
}

echo '<pre>';
$c = merge($a, $b);
var_dump($c);
