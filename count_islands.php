<?php
/**
 0 1 1 0 0 0
 0 1 0 0 1 0
 0 0 0 0 0 0
 0 1 1 1 0 0
 0 0 1 0 0 0
 */
/**
 * 0 0 1 1
 * 0 1 1 1
 * 1 0 1 0
 */
/**
 * 0 1 0 0
 * 0 0 1 0
 * 0 0 0 1
 */
/**
 * 1 1 1
 * 1 0 1
 * 1 1 0
 */
$m = [
    [
        0,
        1,
        1,
        0,
        0,
        0
    ],
    [
        0,
        1,
        0,
        0,
        1,
        0
    ],
    [
        0,
        0,
        0,
        0,
        0,
        0
    ],
    [
        0,
        1,
        1,
        1,
        0,
        0
    ],
    [
        0,
        0,
        1,
        0,
        0,
        0
    ]
];

/**
 *
 * @param integer[][] $m
 * @return integer
 */
function countIslands (array $m) {
    $colsCount = count($m[1]);
    $rowsCount = count($m);
    $result = 0;

    for ($i = 0; $i < $colsCount; $i++) {
        for ($j = 0; $j < $rowsCount; $j++) {
            if ($m[$i][$j] == 1) {
                burnIsland($m, $i, $j);
                $result++;
            }
        }
    }

    return $result;
}

/**
 *
 * @param integer[][] $mSlice
 * @param integer $i
 * @param integer $j
 */
function burnIsland (array &$mSlice, $i, $j) {
    $colsCount = count($mSlice[1]);
    $rowsCount = count($mSlice);
    $mSlice[$i][$j] = 0;

    static $shifts = [
        -1,
        0,
        1
    ];

    foreach ($shifts as $shiftCol) {
        foreach ($shifts as $shiftRow) {
            if ($shiftCol == 0 && $shiftRow == 0 && !isset($mSlice[$i + $shiftCol][$j + $shiftRow])) {
                continue;
            }

            if ($mSlice[$i + $shiftCol][$j + $shiftRow] == 1) {
                $mSlice[$i + $shiftCol][$j + $shiftRow] = 0;
                burnIsland($mSlice, $i + $shiftCol, $j + $shiftRow);
            }
        }
    }
}

echo '<pre>';
$count = countIslands($m);
var_dump($count);
