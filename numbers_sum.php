<?php
error_reporting(E_ALL);

$test1 = [
    1,
    2,
    3,
    4,
    5,
    6,
    5,
    4,
    3,
    2,
    1
];

$test2 = 6;

function main (array $numbers, $etalon) {
    if (empty($numbers)) {
        return [];
    }

    $numbers = array_map('intval', $numbers);
    sort($numbers, SORT_NUMERIC);

    //var_dump('$numbers', $numbers, '$etalon', $etalon);die;
    $result = [];
    $count = count($numbers);

    for ($i = 0; $i < $count; $i++) {
        $numberA = $numbers[$i];

        for ($j = $i + 1; $j < $count; $j++) {
            $numberB = $numbers[$j];

            if ($etalon == $numberA + $numberB) {
                $temp = [
                    $numberA,
                    $numberB
                ];

                $result[] = $temp;
            }
        }
    }

    sort($result);

    $prev = null;
    $result = array_filter($result,
        function  ($r) use( &$prev) {
            if (!is_null($prev)) {
                if ($prev == $r) {
                    return false;
                }
            }
            $prev = $r;
            return true;
        });

    return $result;
    // $result = step($numbers, 0, $etalon);
}
const COUNTS = 10000;
$totalTime = 0.0;

for ($i = COUNTS; $i; $i--) {
    $stime = microtime(true);

    echo '<pre>';
    $pairs = main($test1, $test2);
    $totalTime += microtime(true) - $stime;
    //var_dump('$pairs', $pairs);die;
}

echo sprintf('Execution time: %.06f, avg: %.06f', $totalTime, $totalTime / COUNTS);
