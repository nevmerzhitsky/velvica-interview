<?php
/**
 * Дерево в линейный список.
 */
class Node {

    /**
     *
     * @var string
     */
    public $value = '';

    /**
     *
     * @var Node
     */
    public $next = null;

    /**
     *
     * @var Node[]
     */
    public $bag = [];

    public function __construct ($value, Node $next = null, Node $below = null) {
        $this->value = strval($value);
        $this->next = $next;
        $this->below = $below;
    }

    /**
     * A -> B -> C -> D -> E -> F
     * **** |
     * **** ---> X -> Y -> Z
     */
    public function doFlat (array &$bag = []) {
        if (isset($bag[$this->value])) {
            throw new Exception('Loop found: ' . $this->value);
        }

        $bag[$this->value] = $this;

        if (!is_null($this->below)) {
            $oldNext = $this->next;
            $this->next = $this->below;
            $this->below = null;

            $this->next->getLastNext()->next = $oldNext;
        }

        if (!is_null($this->next)) {
            $this->next->doFlat($bag);
        }
    }

    /**
     *
     * @return Node
     */
    public function getLastNext () {
        if (!is_null($this->next)) {
            return $this->next->getLastNext();
        }

        return $this;
    }
}

function main () {
    $chain = new Node('A',
        new Node('B', new Node('C', new Node('D', new Node('E', new Node('F')))),
            new Node('X', new Node('Y', new Node('Z')))));
    // var_dump($chain);

    $chain->doFlat();
    print_r($chain);
}
main();

/**
 * A -> B -> C -> D -> E -> F
 * **** |
 * **** ---> X -> Y -> Z -> A
 */
/**
 * A -> B -> X -> Y -> Z -> A -> C -> D -> E -> F
 */


/**
 * A
 * |
 * ---> B
 * **** |
 * **** ---> C
 *
 * A -> B
 * **** |
 * **** ---> C
 *
 * A -> B -> C
 */
/**
 * A -> B -> C -> D -> E -> F
 * **** |
 * **** ---> X -> Y -> Z
 */

/**
 * A -> B -> C -> D -> E -> F
 * |
 * ---> X -> Y -> Z
 */

/**
 * A -> B -> C
 * ********* |
 * ********* ---> X -> Y -> Z
 */

/**
 * A -> B -> C -> D -> E -> F
 * **** |
 * **** ---> X -> Y -> D
 *
 * A -> B -> X -> Y -> D -> C -> D -> E -> F
 */
